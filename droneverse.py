#!/usr/bin/env python

"""
User interface for managing designs

# To add a single item
word
Example: daemon
 
# Link two or more nodes
l word link1 link2 link3..
| word link1 link2 link3..
Example: l daemon services
 
# Remove a link
u word link1 link2 link3..
\ word link1 link2 link3..
Example: u daemon services
 
# Save to disk
> filename
w filename
Example: > ~/blah.dot
 
# Load from disk
< filename
Example: < ~/blah.dot
 
# Create a new file
n!
Example: n!

# Tag an item
t daemon has_runtime
t services dbus
t sessions "dbus:/Sessions"
t session "dbus:/Session/:id"
"""

import os

try:
    import gtk
    import pydot
    import webkit
except ImportError:
    print 'Attempting to install missing packages'
    os.system('apt-get install python-pydot graphviz python-webkit python-gtk2')

import gtk
import gobject
import pydot
import tempfile
import sys
import webkit

def setGraphAttr(g,k,v):
    if hasattr(g, 'set_' + k):
        apply(getattr(g, 'set_' + k), [v])

def parseInput(line):
    """
    Stupidly naive input parser for the text entry.
    
    >>> parseInput("cmd arg1 arg2 ?mykey=myval ?another='complex one'")
    ('cmd', ['arg1', 'arg2'], {'mykey':'myval','another':'complex one'})
    
    Return value:
        a three-part tuple containing command, args, kwargs
    """
    line = line.strip()
    args = []
    kwargs = {}
    words = []
    word = ''
    in_quote = None
    for c in line:
        if in_quote and c == in_quote:
            in_quote = None
            word += c
        elif in_quote:
            word += c
        elif c == '"' or c == "'":
            in_quote = c
            word += c
        elif c == ' ':
            words.insert(0, word)
            word = ''
        else:
            word += c
    if word:
        words.insert(0, word)
    for word in words:
        if word.startswith('?'):
            if '=' in word:
                k,v = word[1:].split('=', 1)
            else:
                k = word[1:]
                v = ''
            kwargs[k] = v
        else:
            args.insert(0, word)
    if len(args):
        return args[0], args[1:], kwargs
    return '', args, kwargs

"""
words = [
    "cmd blah blah blah",
    "cmd blah blah ?another='hi there'",
    'cmd blah blah ?item="key this one in"',
]
for w in words:
    print w
    print parseInput(w)
    print
#"""

class CompletionModel(gtk.GenericTreeModel):
    def __init__(self, getGraphFunc, getTextFunc):
        gtk.GenericTreeModel.__init__(self)
        assert getGraphFunc
        if not getTextFunc:
            getTextFunc = lambda: ''
        self.getGraph = getGraphFunc
        self.getText = getTextFunc
        self.defaults = ['?help', '?color=', '?label=', '?comment=',]
        self.count = 0
        self.cache_count()
        
    def on_get_flags(self):
        return gtk.TREE_MODEL_LIST_ONLY
        
    def on_get_n_columns(self):
        return 2
        
    def on_get_column_type(self, index):
        return str
        
    def on_get_iter(self, path):
        return path[0]
        
    def on_get_path(self, iter):
        return (iter,)
        
    def on_get_value(self, iter, index):
        i = 0
        for item in self.defaults:
            if i == iter:
                return item
            i += 1
        for item in self.walk_graph():
            if i == iter:
                if index == 0:
                    return item
                elif index == 1:
                    if type(item) is str:
                        text = item
                    elif hasattr(item, 'get_name'):
                        text = item.get_name()
                    else:
                        return ''
                    prefix = self.getText()
                    if prefix.endswith(' '):
                        return prefix + item
                    elif ' ' in prefix:
                        r = ' '.join(prefix.split(' ')[:-1]) + ' ' + text
                        return r
                    else:
                        return text
                return ''
            i += 1
        return ''
        
    def walk_graph(self):
        graph = self.getGraph()
        if not graph:
            return
        
        def join(*items):
            for subset in items:
                for item in subset:
                    yield item
        
        # yield items and keep count for cache
        count = 0
        for item in join(graph.obj_dict['nodes'], graph.obj_dict['edges']):
            count += 1
            yield item
        count += len(self.defaults)
        old_count = self.count
        self.count = count
        
        # Fixme: if we have a different size model than before, we need
        #   to notify so that the completion can complete on them.
        if old_count != count:
            pass
        
    def cache_count(self):
        for i in self.walk_graph():
            pass

    def on_iter_next(self, iter):
        iter += 1
        if iter < self.count:
            return iter
        return None
        
    def on_iter_children(self, iter):
        return None
        
    def on_iter_has_child(self, iter):
        return False
        
    def on_iter_n_children(self, iter):
        if not iter:
            return len(list(self.walk_graph())) or 1
        return 0
        
    def on_iter_nth_child(self, parent, n):
        if not parent:
            return self.on_get_iter((n,))
        
    def on_iter_parent(self, iter):
        return None
    
class DroneverseView(gtk.Table):
    def __init__(self):
        gtk.Table.__init__(self)
        
        self.scroller = gtk.ScrolledWindow()
        self.scroller.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.attach(self.scroller, 1, 2, 1, 2)
        self.scroller.show()

        # add image view and scroller to zoom

class DroneverseWindow(gtk.Window):
    graph = None
    selected_node = None
    
    def __init__(self):
        gtk.Window.__init__(self)
        self.graph = pydot.Dot()
        self.raw_data = self.graph.create_dot()
        self.selected = ''
        self.zoom = 1
        self.tags = {}
        self.tag_colors = {
            'bin':    [('fillcolor', '#ffd099'), ('color', 'orange'), ('style', 'filled')],
            'dbus':   [('fillcolor', '#729fcf'), ('color', '#265687'), ('style', 'filled')],
            'method': [('fillcolor', 'purple'), ('style', 'filled')],
            'prop':   [('fillcolor', 'green'), ('style', 'filled')],
        }
        
        # window defaults
        self.props.title = 'Droneverse'
        self.set_default_size(800, 600)
        
        # main container
        self.main_box = gtk.VBox()
        self.add(self.main_box)
        self.main_box.show()

        ag = gtk.AccelGroup()
        self.add_accel_group(ag)


        # input box for commands
        self.entry = gtk.Entry()
        self.entry.add_accelerator('grab-focus', ag, ord('l'), gtk.gdk.CONTROL_MASK, 0)
        self.main_box.pack_start(self.entry, False, True, 0)
        self.entry.connect('activate', self.entry_activate)
        tab = gtk.gdk.keyval_from_name('Tab')
        esc = gtk.gdk.keyval_from_name('Escape')
        def onKeyPress(w,e):
            if e.keyval == tab:
                def gotoEnd():
                    w.grab_focus()
                    w.set_position(-1)
                gobject.timeout_add(0, gotoEnd)
            #elif e.keyval == esc:
            #    start = w.props.cursor_position
            #    end = w.props.selection_bound
            #    if end < start:
            #        start, end = end, start
            #    t = w.props.text
            #    w.props.text = t[:start] + t[end:]
            #    w.set_position(-1)
        self.entry.set_events(gtk.gdk.KEY_PRESS_MASK)
        self.entry.connect('key-press-event', onKeyPress)
        completion = gtk.EntryCompletion()
        completion.props.text_column = 1
        m = self.create_completion()
        c = gtk.CellRendererText()
        completion.pack_start(c, True)
        completion.add_attribute(c, 'text', 1)
        completion.set_model(m)
        completion.props.inline_completion = True
        #completion.props.inline_selection = True
        completion.props.minimum_key_length = 2
        self.entry.set_completion(completion)
        self.entry.show()
        
        # infobar
        self.infobar = GtkInfoBar()
        self.infobar.parent(self.main_box)
        self.message = gtk.Label('')
        self.message.props.xalign = 0.0
        self.message.show()
        self.infobar.set_message_type(gtk.MESSAGE_ERROR)
        self.infobar.add_to_content_area(self.message)
        
        # viewing of the design
        #self.view = DroneverseView()
        #self.main_box.pack_start(self.view, True, True, 0)
        #self.view.show()
        
        def onLoadFinished(v, b):
            if hasattr(self, 'pending'):
                if self.pending:
                    self.pending.close()
                    self.pending = None
            v.set_zoom_level(1)
            # update auto-complete cache
            self.completion_model.cache_count()
        self.webkit = webkit.WebView()
        self.main_box.pack_start(self.webkit, True, True, 0)
        self.webkit.connect('load-finished', onLoadFinished)
        self.show_help()
        self.webkit.show()
        
        """
        self.image = gtk.Image()
        self.modify_bg(gtk.STATE_NORMAL, gtk.gdk.Color(0xFFFF, 0xFFFF, 0xFFFF))
        self.main_box.pack_start(self.image, True, True, 0)
        self.image.show()
        """
        
        scroller = gtk.ScrolledWindow()
        scroller.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.main_box.pack_start(scroller, False, True, 0)
        scroller.show()
        
        self.tv = gtk.TextView()
        self.tv.props.editable = False
        scroller.add(self.tv)
        self.tv.show()
        
    def show_help(self):
        self.webkit.load_string("""
            <html>
             <style>table tr th { text-align: left; }</style>
             <body>
              <center>Enter some text in the entry above.</center>
              <br/>
              <table width="100%">
               <tr>
                <th>Command</th>
                <th>Arguments</th>
                <th>Description</th>
               </tr>
               <tr>
                 <td>?help</td>
                 <td></td>
                 <td>Show this help menu.</td>
               </tr>
               <tr>
                <td>q or quit</td>
                <td></td>
                <td>Quit the application</td>
               </tr>
               <tr>
                <td>q! or quit!</td>
                <td></td>
                <td>Quit the application now, do not pass go, do not collect
                 $200.00.</td>
               </tr>
               <tr>
                <td>n!</td>
                <td></td>
                <td>Create a new document</td>
               </tr>
               <tr>
                <td>w or &gt;</td>
                <td><i>Filename</i> [?format=png|dot|svg]</td>
                <td>Write the file to <i>filename</i></td>
               </tr>
               <tr>
                <td>&lt;</td>
                <td><i>Filename</i></td>
                <td>Loads from a graphviz dot file</td>
               </tr>
               <tr>
                <td>[a] [<i>word</i>..]</td>
                <td></td>
                <td>Creates new node(s). "Quotes are Okay". This is the default
                 command so the prefix 'a' is optional.
                </td>
               </tr>
               <tr>
                <td>l or |</td>
                <td>
                 <i>src</i> [<i>dst</i>...]
                 [<span style="color:red;">?color=red</span> ?label=label]
                </td>
                <td>Creates one or more links between nodes.</td>
               </tr>
               <tr>
                <td>u or \</td>
                <td><i>src</i> [<i>dst</i>...]</td>
                <td>Removes one or more links between nodes.</td>
               </tr>
               <tr>
                <td>d or x</td>
                <td>[<i>node</i>...]</td>
                <td>Delete one or more nodes</td>
               </tr>
               <tr>
                <td>r</td>
                <td></td>
                <td>Force a re-rendering of the diagram.</td>
               </tr>
               <tr>
                <td>t or tag</td>
                <td>node tags...</td>
               </tr>
               <tr>
                <td>dt or deltag</td>
                <td>node tags...</td>
               </tr>
              </table>
             </body>
            </html>
            """, 'text/html', 'utf-8', 'file:///')
        
    def entry_activate(self, entry):
        if self.evaluate(entry.get_text()):
            b = self.tv.get_buffer()
            b.place_cursor(b.get_end_iter())
            b.insert_at_cursor(entry.get_text() + '\n')
            entry.set_text('')
            self.tv.scroll_to_iter(b.get_end_iter(), 0.0)
            self.tv.queue_draw() # ugh
            if hasattr(self, 'orig_base'):
                entry.modify_base(gtk.STATE_NORMAL, self.orig_base)
                entry.modify_fg(gtk.STATE_NORMAL, self.orig_fg)
        else:
            if not hasattr(self, 'orig_color'):
                self.orig_base = entry.style.base[gtk.STATE_NORMAL]
                self.orig_fg = entry.style.fg[gtk.STATE_NORMAL]
            new_base = entry.style.bg[gtk.STATE_SELECTED]
            new_fg = entry.style.bg[gtk.STATE_SELECTED]
            entry.modify_base(gtk.STATE_NORMAL, new_base)
            entry.modify_fg(gtk.STATE_NORMAL, new_fg)
        
    def evaluate(self, data):
        if data.strip() == '?help':
            self.show_help()
            return True
        
        success = False
        command, args, kwargs = parseInput(data)
        ex = None
        msg = ''
        
        if command in ('l', '|', 'link'):
            if not len(args[1:]):
                ex = ValueError('Not enough arguments')
            else:
                for arg in args:
                    if arg not in self.graph.obj_dict['nodes']:
                        node = pydot.Node()
                        node.set_name(arg)
                        self.graph.add_node(node)
                src = args[0]
                for dst in args[1:]:
                    edge = pydot.Edge()
                    edge.obj_dict['points'] = (src, dst)
                    for k,v in kwargs.iteritems():
                        setGraphAttr(edge, k, v)
                    self.graph.add_edge(edge)
                msg = "Linked node '%s' to %s." % (src, str(args[1:]))
                self.selected = src
                success = True
        
        elif command in ('\\', 'u', 'unlink'):
            if not len(args[1:]):
                ex = ValueError('Not enough arguments')
            else:
                src = args[0]
                to_remove = []
                for dst in args[1:]:
                    for edge in self.graph.get_edges():
                        points = edge.get_source(), edge.get_destination()
                        if src in points and dst in points:
                            to_remove.insert(0, points)
                for rmv in to_remove:
                    del self.graph.obj_dict['edges'][rmv]
                msg = "Unlinked node '%s' from %s" % (src, str(args[1:]))
                success = True
        
        elif command in ('ls',):
            if not args:
                dirent = os.getcwd()
            else:
                dirent = ' '.join(args)
            items = ['"%s"' % s for s in os.listdir(dirent)]
            node = pydot.Node()
            name = '"ls (%s)"' % dirent
            node.set_name(name)
            self.graph.add_node(node)
            for item in items:
                edge = pydot.Edge()
                for k,v in kwargs.iteritems():
                    setGraphAttr(edge, k, v)
                edge.obj_dict['points'] = (name, item)
                self.graph.add_edge(edge)
            self.selected = name
            success = True
        
        elif command in ('d', 'x'):
            for name in args:
                if name in self.graph.obj_dict['nodes']:
                    del self.graph.obj_dict['nodes'][name]
                remove_points = []
                for points in self.graph.obj_dict['edges']:
                    if name in points:
                        remove_points.insert(0, points)
                for points in remove_points:
                    del self.graph.obj_dict['edges'][points]
            msg = "Deleted node '%s'." % name
            success = True
        
        elif command in ('n!',):
            self.graph = pydot.Dot()
            self.raw_data = self.graph.create_dot()
            self.tags = {}
            success = True
        
        elif command in ('w', '>'):
            path = os.path.expanduser(' '.join(args))
            if 'format' in kwargs:
                format = kwargs['format']
            else:
                format = 'dot'
            f = file(path + '.new', 'w')
            creator = getattr(self.graph, 'create_' + format)
            f.write(creator())
            # now add our node tags
            f.write('\n/* __DRONEVERSE_TAGS_BEGIN__ */\n')
            def escape(s):
                s = s.replace(',', '\,')
                s = s.replace('\n', '\\n')
                return s
            for node,tags in self.tags.iteritems():
                f.write('%s %s\n' % (node, ','.join([escape(a) for a in tags])))
            f.write('/* __DRONEVERSE_TAGS_END__ */\n')
            f.flush()
            f.close()
            os.rename(path + '.new', path)
            msg = "Wrote diagram to '%s' as %s document." % (path, format)
            success = True
        
        elif command in ('<',):
            path = os.path.expanduser(' '.join(args))
            d = file(path).read()
            self.graph = pydot.graph_from_dot_data(d)
            # parse tags
            def unescape(t):
                words = []
                lastSlash = False
                word = ''
                for c in t:
                    if c == '\\':
                        lastSlash = True
                    elif not lastSlash and c in (',',):
                        words.append(word)
                        word = ''
                    elif (lastSlash and c in (',', 'n')) or not lastSlash:
                        word += c
                        lastSlash = False
                    else:
                        print 'unknown escape', c
                if word:
                    words.append(word)
                return words
            inTags = False
            for line in d.split('\n'):
                line = line.strip()
                if line == '/* __DRONEVERSE_TAGS_BEGIN__ */':
                    inTags = True
                elif line == '/* __DRONEVERSE_TAGS_END__ */':
                    inTags = False
                elif inTags:
                    node, tags = line.split(' ', 1)
                    self.tags[node] = unescape(tags)
            msg = "Diagram loaded from '%s'." % path
            success = True
        
        elif command in ('r',):
            # redraw
            success = True
            
        elif command in ('q!', 'quit!'):
            self.props.sensitive = False
            self.hide()
            gtk.main_quit()
            return True
            
        elif command in ('q', 'quit'):
            self.infobar.set_message_type(gtk.MESSAGE_QUESTION)
            self.message.set_text('Do you really want to quit?')
            h = gtk.HBox(True, 6)
            h.set_size_request(150, -1)
            ok = gtk.Button(stock=gtk.STOCK_OK)
            cancel = gtk.Button(stock=gtk.STOCK_CANCEL)
            h.pack_end(ok, False, True, 0)
            h.pack_end(cancel, False, True, 0)
            fake = gtk.Label()
            fake.show()
            h.pack_start(fake, True, True, 0)
            gobject.timeout_add(0, lambda: cancel.grab_focus())
            h.show_all()
            def cleanup(b, do_quit=False):
                h.hide()
                h.destroy()
                self.infobar.hide()
                if do_quit:
                    self.props.sensitive = False
                    self.hide()
                    gtk.main_quit()
                    sys.exit(0)
                else:
                    self.entry.set_text('')
                    self.entry.grab_focus()
            cancel.connect('clicked', cleanup)
            ok.connect('clicked', cleanup, True)
            self.infobar.add_to_content_area(h)
            self.infobar.show()
            return True
            
        elif command == 'f':
            self.selected = len(args) and args[0] or ''
            success = True
        
        elif command in ('z', 'zoom'):
            if not len(args):
                msg = 'Zoom: in, out, one'
                success = True
            else:
                success = True
                if args[0] in ('in',):
                    self.zoom /= .75
                elif args[0] in ('out',):
                    self.zoom *= .75
                elif args[0] in ('one',):
                    self.zoom = 1
                elif args[0] == 'two':
                    self.zoom = 2
                else:
                    try:
                        self.zoom = float(args[0])
                    except ValueError:
                        ex = ValueError('"%s" is not a valid zoom command.' % args[0])
                        success = False
                
                if not ex:
                    msg = 'Zoom at %f' % self.zoom

        elif command in ('t', 'tag'):
            if not len(args[1:]):
                msg = 'Tag: node tags...'
                success = True
            else:
                node = args[0]
                if node not in self.tags:
                    self.tags[node] = []
                self.tags[node].extend(args[1:])
                msg = 'Tagged %s with %s' % (args[0], args[1:])
                success = True

        elif command in ('dt', 'deltag'):
            if not len(args[1:]):
                msg = 'Delete tag: node tags...'
                success = True
            else:
                node = args[0]
                try:
                    dotnode = self.graph.get_node(node)
                except:
                    dotnode = None
                if node in self.tags:
                    for t in args[1:]:
                        try:
                            if dotnode and t in self.tag_colors:
                                for k,v in self.tag_colors[t]:
                                    apply(getattr(dotnode, 'set_' + k), [''])
                            self.tags[node].remove(t)
                        except Exception, ex:
                            print ex
                success = True
                msg = 'Removed tags %s from %s' % (args[1:], node)

        else:
            # default command is to create new nodes
            if command not in ('a', 'add', '^') or not len(args):
                args.insert(0, command)
            
            for name in args:
                node = pydot.Node()
                node.set_name(name)
                for k,v in kwargs.iteritems():
                    setGraphAttr(node, k, v)
                self.graph.add_node(node)
                self.select_node(node)
            
            success = True
            self.selected = args[0]
        
        try:
            if not success and ex:
                raise ex
            self.generate()
            self.infobar.set_message_type(gtk.MESSAGE_INFO)
            if msg:
                self.message.set_text(msg)
                self.infobar.show()
            else:
                self.infobar.hide()
            return success
        except Exception, ex:
            msg = ex.__class__.__name__ + ': ' + str(ex)
            self.infobar.set_message_type(gtk.MESSAGE_ERROR)
            self.message.set_text(msg)
            self.infobar.show()
            return False

    def colorize_from_tags(self):
        for node in self.graph.get_nodes():
            name = node.get_name()
            if name in self.tags:
                for tag in self.tags[name]:
                    shorty = tag.split(':')[0]
                    if shorty in self.tag_colors:
                        for attr,val in self.tag_colors[shorty]:
                            apply(getattr(node, 'set_' + attr), [val])
        
    def select_node(self, node):
        self.selected = node
        
    def create_completion(self):
        getGraph = lambda: self.graph
        getText = lambda: self.entry.get_text()
        self.completion_model = CompletionModel(getGraph, getText)
        return self.completion_model
        
    def generate(self):
        # Force a retagging from changes
        self.colorize_from_tags()

        stream = tempfile.NamedTemporaryFile()
        
        # write content to disk
        try:
            DPI = 96
            
            # alter size and zoom to fit our window
            _, _, width, height = self.webkit.get_allocation()
            if self.message.props.visible:
                height -= 35 # space for message bar
            width = width * (float(72) / DPI)
            height = height * (float(72) / DPI)
            zoom = self.zoom
            center = self.selected
            viewport = '%d,%d,%f' % (width, height, zoom)
            if center:
                viewport += ",'" + center + "'"
            
            # alter viewport, and restore afterwards
            try:
                orig_viewport = self.graph.get_viewport()
            except:
                orig_viewport = ''
            self.graph.set_viewport(viewport)
            raw_data = self.graph.create_dot()
            raw_png = self.graph.create_png()
            self.graph.set_viewport(orig_viewport)
            
            stream.write(raw_png)
            stream.flush()
            
            self.pending = stream
            self.webkit.open('file://' + stream.name)
        except:
            # restore previous state
            self.graph = pydot.graph_from_dot_data(self.raw_data)
            stream.close()
            raise
        
        # store our now validated data
        self.raw_data = raw_data

def nastyJungleOfLoveHack(parent):
    # holy hell i should be murdered for this
    x = str(parent).split(' at ')[2]
    x = int(x[:x.index(')')], 16)
    return x
    
# work around infobar not yet available
class GtkInfoBar(object):
    def __init__(self):
        import ctypes
        try:
            self.libgtk = ctypes.CDLL('libgtk-x11-2.0.so')
        except:
            # seems to be needed for some dynamic loaders
            self.libgtk = ctypes.CDLL('libgtk-x11-2.0.so.0')
        self.pointer = self.libgtk.gtk_info_bar_new()
    
    def show(self):
        self.libgtk.gtk_widget_show(self.pointer)
        
    def hide(self):
        self.libgtk.gtk_widget_hide(self.pointer)
        
    def parent(self, parent, use_pack=True):
        x = nastyJungleOfLoveHack(parent)
        if use_pack:
            self.libgtk.gtk_box_pack_start(x, self.pointer, False, True, 0)
        else:
            self.libgtk.gtk_container_add(x, self.pointer)
            
    def add_to_content_area(self, w):
        x = nastyJungleOfLoveHack(w)
        area = self.libgtk.gtk_info_bar_get_content_area(self.pointer)
        self.libgtk.gtk_container_add(area, x)
    
    def set_message_type(self, t):
        self.libgtk.gtk_info_bar_set_message_type(self.pointer, int(t))

if __name__ == '__main__':
    import gobject; gobject.threads_init()
    win = DroneverseWindow()
    win.connect('delete-event', gtk.main_quit)
    win.show()
    gtk.main()
