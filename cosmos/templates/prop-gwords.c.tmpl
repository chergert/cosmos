% if prop.readable:
/**
 * ${program.lower_name}_${klass.lower_name}_get_${prop.lower_name}:
 * @${klass.lower_name}: A #${program.camel_name}${klass.camel_name}
 *
 * Retreives the "${prop.name}" property.
% if 'threading' in klass.features:
 *
 * This method is thread-safe.
 *
% endif
 * Return value: a ${prop.field_type}
 */
${prop.field_type}
${program.lower_name}_${klass.lower_name}_get_${prop.lower_name} (${program.camel_name}${klass.camel_name}* ${klass.lower_name})
{
% if 'threading' in klass.features:
	${program.camel_name}${klass.camel_name}Private *priv;
	${prop.field_type} ${prop.lower_name}_copy;

	g_return_val_if_fail (${program.upper_name}_IS_${klass.upper_name} (${klass.lower_name}), NULL);

	priv = ${klass.lower_name}->priv;

	g_static_rw_lock_reader_lock (&priv->rw_lock);
	${prop.lower_name}_copy = g_strdup (priv->${prop.lower_name});
	g_static_rw_lock_reader_unlock (&priv->rw_lock);

	return ${prop.lower_name}_copy;
% else:
	g_return_val_if_fail (${program.upper_name}_IS_${klass.upper_name} (${klass.lower_name}), 0);
	return ${klass.lower_name}->priv->${prop.lower_name};
% endif
}
% endif
% if prop.writable:

/**
 * ${program.lower_name}_${klass.lower_name}_set_${prop.lower_name}:
 * @${klass.lower_name}: A #${program.camel_name}${klass.camel_name}
 * @${prop.lower_name}: A #${prop.field_type}
 *
 * Sets the "${prop.name}" property.
 */
void
${program.lower_name}_${klass.lower_name}_set_${prop.lower_name} (${program.camel_name}${klass.camel_name}* ${klass.lower_name},
${program.lower_space} ${klass.lower_space}     ${prop.lower_space}  ${prop.field_type} ${prop.lower_name})
{
	${program.camel_name}${klass.camel_name}Private *priv;

	g_return_if_fail (${program.upper_name}_IS_${klass.upper_name} (${klass.lower_name}));

	priv = ${klass.lower_name}->priv;

% if 'threading' in klass.features:
	g_static_rw_lock_writer_lock (&priv->rw_lock);
% endif
	priv->${prop.lower_name} = ${prop.lower_name};
% if 'threading' in klass.features:
	g_static_rw_lock_writer_unlock (&priv->rw_lock);
% endif

	g_object_notify (G_OBJECT (${klass.lower_name}), "${prop.name}");
}
% endif

<% state['prototype'] = "%s %s_%s_get_%s (%s%s *%s);\nvoid %s_%s_set_%s (%s%s *%s, %s %s);" % (
                        prop.field_type,
                        program.lower_name,
			klass.lower_name,
			prop.lower_name,
			program.camel_name,
			klass.camel_name,
			klass.lower_name,
                        program.lower_name,
			klass.lower_name,
			prop.lower_name,
			program.camel_name,
			klass.camel_name,
			klass.lower_name,
			prop.field_type,
			prop.lower_name
			) %>
