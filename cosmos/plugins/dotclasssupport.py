#!/usr/bin/env python

#
# This file belongs to humanity and ever other intellegent life form
# that can be found in the cosmos.  It's distribution is unlimited
# and unrestricted.  The ideas provided within this file are owned by no-one
# as they are a product of everyone and every state the universe ever was
# and ever will be.
#

"""
look for classes in the dotgraph.
"""

from cosmos                  import interfaces
from cosmos.plugins.dotgraph import *
from zope.interface          import implements
from twisted.plugin          import IPlugin
from twisted.python          import log

class DotGraphClassSupport(DotGraphSupportBase):
    # the project we are building
    project = None

    def do_support(self, graph, project):
        # make sure the class plugin has run
        ensureSupported('dotgraphprojecthelpers.ProjectSupport', graph, project)

        self.project = project
        self.graph = graph
        for node in dotwalk(graph):
            # sometimes, i add classes by tagging them
            if has_tag(node, ['struct', 'class', 'service']): # service is implicit
                self.addClass(node)

            # sometimes, they point to methods
            elif looks_upon_tagged(node, 'method') or looks_upon_tagged(node, 'prop'):
                self.addClass(node)

    def addClass(self, node):
        if has_tag(node, 'struct'):
            # this is really a lightweight struct
            cls = DotGraphClass(name=node.get_name().replace('_','-'))
            cls.node = node
            self.project.structs.append(cls)
        else:
            cls = DotGraphClass(name=node.get_name().replace('_','-'))
            cls.node = node
            self.project.classes.append(cls)

        # we need to figure out what executables can see us
        names = [get_tag(get_node(self.graph, n), 'name') or n
                 for n in who_gazes_upon_me_tagged(node, ('bin', 'lib'))]
        cls.visible_by = names

        # lets see if we have any properties. they would be neighbors that have
        # the 'prop' tag.
        for child in i_see_tagged(node, ('prop',)):
            self.addProperty(cls, child)

        for child in i_see_tagged(node, ('method',)):
            self.addMethod(cls, child)

    def addProperty(self, klass, node):
        prop = DotGraphProperty()
        prop.name = node.get_name().replace('_','-')
        prop.type_name = get_tag(node, 'type')
        prop.readable = True
        prop.writable = True
        klass.properties[prop.name] = prop

    def addMethod(self, klass, node):
        method = DotGraphMethod()
        method.name = node.get_name().replace('_','-')
        method.type_name = get_tag(node, 'type')
        klass.methods.append(method)

DotGraphClassSupport = DotGraphClassSupport()

class DotGraphClass(object):
    implements(interfaces.Class)

    name = ''
    methods = None
    features = None
    properties = None

    def __init__(self, **kwargs):
        self.methods = []
        self.features = {}
        self.properties = {}
        self.__dict__.update(kwargs)

    def prepare(self):
        self.lower_name = self.name.lower().replace('-','_')
        self.upper_name = self.name.upper().replace('-','_')
        self.camel_name = ''.join([c.capitalize() for c in self.name.split('-')])
        self.lower_space = ' ' * len(self.lower_name)
        self.upper_space = ' ' * len(self.upper_name)
        self.camel_space = ' ' * len(self.camel_name)
        self.type_name = 'TYPE_' + self.upper_name

class DotGraphProperty(object):
    implements(interfaces.Property)

    name = ''
    type_name = 'string'
    readable = True
    writable = True
    features = ''

    def prepare(self):
        self.lower_name = self.name.lower().replace('-','_')
        self.upper_name = self.name.upper().replace('-','_')
        self.camel_name = ''.join([c.capitalize() for c in self.name.split('-')])
        self.lower_space = ' ' * len(self.lower_name)
        self.upper_space = ' ' * len(self.upper_name)
        self.camel_space = ' ' * len(self.camel_name)

    def __repr__(self):
        return '<Property(%s %s [%s%s])>' % (self.type_name, self.name, self.readable and 'R' or '', self.writable and 'W' or '')

class DotGraphMethod(object):
    implements(interfaces.Method)

    name = ''
    type_name = 'void'
    features = ''

    def prepare(self):
        self.lower_name = self.name.lower().replace('-','_')
        self.upper_name = self.name.upper().replace('-','_')
        self.camel_name = ''.join([c.capitalize() for c in self.name.split('-')])
        self.lower_space = ' ' * len(self.lower_name)
        self.upper_space = ' ' * len(self.upper_name)
        self.camel_space = ' ' * len(self.camel_name)

    def __repr__(self):
        return '<Method(%s %s)>' % (self.type_name, self.name)
