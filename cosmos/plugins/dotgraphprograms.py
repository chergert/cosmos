#!/usr/bin/env python

#
# This file belongs to humanity and ever other intellegent life form
# that can be found in the cosmos.  It's distribution is unlimited
# and unrestricted.  The ideas provided within this file are owned by no-one
# as they are a product of everyone and every state the universe ever was
# and ever will be.
#

from cosmos.interfaces       import Class
from cosmos.plugins.dotgraph import *
from twisted.plugin          import IPlugin
from zope.interface          import implements

class DotGraphPrograms(DotGraphSupportBase):
    def do_support(self, graph, project):
        self.project = project
        self.graph = graph

        # i tag my programs as `bin' typically
        for node in dotwalk(graph):
            if has_tag(node, 'bin'):
                self.addProgram(node)

    def addProgram(self, node):
        program = DotProgram(name=node.get_name(), project=self.project)

        if has_tag(node, 'plugins'):
            program.features['plugins'] = ''

        if has_tag(node, 'name'):
            name = get_tag(node, 'name')
            program.name = name

        if has_tag(node, 'prefix'):
            program.prefix = get_tag(node, 'prefix')
        else:
            program.prefix = program.name.lower().replace('_','-')

        # how do we determine if we need threading?
        program.features['threading'] = ''
        program.features['dbus'] = ''
        program.features['services'] = ''
        if has_tag(node, 'gtk'):
            program.features['gtk'] = ''

        node.project.programs[program.name] = program

DotGraphPrograms = DotGraphPrograms()

class DotProgram(object):
    implements(Class)

    bin_type = 'program'
    name = ''
    license = 'GPL-3'
    classes = None
    features = {}
    services = {}

    def __init__(self, **kwargs):
        self.classes = {}
        self.__dict__.update(kwargs)

    def prepare(self):
        self.lower_name = self.prefix.lower().replace('-','_')
        self.upper_name = self.lower_name.upper()
        self.camel_name = ''.join([a.capitalize() for a in self.prefix.split('_')])
        self.lower_space = ' ' * len(self.lower_name)
        self.upper_space = ' ' * len(self.upper_name)
        self.camel_space = ' ' * len(self.camel_name)

        # find all the services
        for cls in self.project.classes:
            print cls
            if has_tag(cls.node, 'service'):
                print cls.node, 'has tag'
                self.services[cls.name] = cls
