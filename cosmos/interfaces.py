#!/usr/bin/env python

#
# This file belongs to humanity and ever other intellegent life form
# that can be found in the cosmos.  It's distribution is unlimited
# and unrestricted.  The ideas provided within this file are owned by no-one
# as they are a product of everyone and every state the universe ever was
# and ever will be.
#

from zope.interface import Interface, Attribute

class ProjectWriter(Interface):
    Attribute('buildSystem', 'The build system to use')

    def write(self, ast):
        pass

    def configure(self, **kwargs):
        pass

class BuildSystem(Interface):
    def prepare(self, ast):
        # prepare the build system for serializing a project.
        pass

    def fileFor(self, name, mode='rw'):
        # create a file and return its stream for the file @name
        # if the mode contains 'x' it will also be marked as
        # executable.
        pass

    def complete(self):
        # complete the writing of any files that may still need
        # to be written. such as configure, authors, etc.
        pass

class InFileParser(Interface):
    # interface to parse the FILENAME.. from command line
    # and generate a project ast.  They will need type discovery
    # in the future too.

    def parse(self, fileName):
        pass

    def canParse(self, fileName):
        pass

class Project(Interface):
    # interface for projects
    Attribute('name', 'The project name')
    Attribute('slogan', 'The project slogan')
    Attribute('license', 'The default license')
    Attribute('authors', 'The authors of the project')
    Attribute('programs', 'The programs in the project')
    Attribute('libraries', 'The libraries in the project')

class Program(Interface):
    Attribute('name', 'The program name')
    Attribute('classes', 'The programs classes')

class Class(Interface):
    Attribute('name', 'The class name')
    Attribute('methods', 'The class methods')
    Attribute('properties', 'The class properties')

class Property(Interface):
    Attribute('name', 'The property name')
    Attribute('readable', 'If the property is readable')
    Attribute('writable', 'If the property is writable')

class Method(Interface):
    Attribute('name', 'The property name')
