#!/usr/bin/env python

#
# This file belongs to humanity and ever other intellegent life form
# that can be found in the cosmos.  It's distribution is unlimited
# and unrestricted.  The ideas provided within this file are owned by no-one
# as they are a product of everyone and every state the universe ever was
# and ever will be.
#

from   __future__               import with_statement
from   cosmos                   import rawTemplateFor, templateFor
from   cosmos.interfaces        import ProjectWriter
from   cosmos.plugins.autotools import Autotools
import os
import time
from   zope.interface           import implements

class CProjectWriter(object):
    implements(ProjectWriter)

    buildSystem = None

    def __init__(self):
        self.buildSystem = Autotools()

    def write(self, project):
        self.project = project

        self.buildSystem.addFeature('lang:c')
        self.buildSystem.addFeature('doc:gtk')
        self.buildSystem.addFeature('dbus')
        self.buildSystem.addFeature('i18n')

        self.buildSystem.prepare(project)

        for prog in project.programs.itervalues():
            prog.prepare()
            self.writeProgram(prog)

        for lib in project.libraries:
            print lib

        print 'Your project "%s" has been generated!' % project.name

        self.buildSystem.complete()

    def writeProgram(self, program):
        self.buildSystem.addDirectory(program.name)
        self.writeVersionHeader(program)

        if 'i18n' not in program.features:
            program.features['i18n'] = ''

        # write our file containing main()
        self.writeMain(program)

        # write pathing helpers
        self.writePaths(program)

        # write our runtime management
        self.writeRuntime(program)

        # if we need services, generate the iface
        if 'services' in program.features:
            self.writeService(program)

        # generate each of our classes
        for klass in self.project.classes:
            if program.name in klass.visible_by:
                self.writeClass(program, klass)

        # generate each of our structs
        for klass in self.project.structs:
            if program.name in klass.visible_by:
                self.writeStruct(program, klass)

    def writeService(self, prog):
        p = prog.name
        pc = '%s/%s-service.c' % (p,prog.prefix)
        ph = '%s/%s-service.h' % (p,prog.prefix)
        with self.buildSystem.fileFor(pc) as f:
            self.writeHeader(prog, pc, f)
            t = templateFor('service.c.tmpl')
            d = t.render(program=prog)
            f.write(d.strip())

        with self.buildSystem.fileFor(ph) as f:
            self.writeHeader(prog, ph, f)
            t = templateFor('service.h.tmpl')
            d = t.render(program=prog)
            f.write(d.strip())

    def writeRuntime(self, prog):
        p = prog.name
        pc = '%s/%s-runtime.c' % (p,prog.prefix)
        ph = '%s/%s-runtime.h' % (p,prog.prefix)
        with self.buildSystem.fileFor(pc) as f:
            self.writeHeader(prog, pc, f)
            t = templateFor('runtime.c.tmpl')
            d = t.render(program=prog)
            f.write(d.strip())

        with self.buildSystem.fileFor(ph) as f:
            self.writeHeader(prog, ph, f)
            t = templateFor('runtime.h.tmpl')
            d = t.render(program=prog)
            f.write(d.strip())

    def writePaths(self, prog):
        p = prog.name
        pc = '%s/%s-paths.c' % (p,prog.prefix)
        ph = '%s/%s-paths.h' % (p,prog.prefix)
        with self.buildSystem.fileFor(pc) as f:
            self.writeHeader(prog, pc, f)
            t = templateFor('paths.c.tmpl')
            d = t.render(program=prog)
            f.write(d.strip())

        with self.buildSystem.fileFor(ph) as f:
            self.writeHeader(prog, ph, f)
            t = templateFor('paths.h.tmpl')
            d = t.render(program=prog)
            f.write(d.strip())

    def writeVersionHeader(self, program):
        verpath = program.name+'/'+program.prefix+'-version.h.in'
        with self.buildSystem.fileFor(verpath) as f:
            d = rawTemplateFor('version.h.in').read()
            d = d.replace('##NAME##', program.name.upper().replace('-','_'))
            d = d.replace('##Name##',
                          ''.join([c.capitalize() for c in
                                   program.name.split('-')]))
            d = d.replace('##-name##', program.name)
            d = d.replace('##_name##', program.name.lower().replace('-','_'))
            f.write(d)

    def writeMain(self, prog):
        p = '%s/main.c' % (prog.name,)
        with self.buildSystem.fileFor(p) as f:
            def w(*a):
                f.write(' '.join(a) + '\n')
            self.writeHeader(prog, p, f)
            t = templateFor('main.c')
            d = t.render(project=self.project, program=prog)
            w(d)

    def writeMethod(self, **kwargs):
        data = ''
        if kwargs.get('docs'):
            pass
        if kwargs.get('static'):
            data += 'static '
        if kwargs.get('retval'):
            data += kwargs.get('retval') + '\n'
        else:
            data += 'void\n'
        data += kwargs.get('name') + ' ('
        if not kwargs.get('args'):
            data += 'void'
        else:
            a = kwargs.get('args', [])
            l = max([len(t) for t,_ in a])
            s = max([len([c for c in s if c == '*']) for _,s in a])
            print s
            isfirst = True
            for t,n in a:
                space = (' ' * (l-len(t)))
                if isfirst:
                    line = ''
                else:
                    line = ',\n'
                    line += ' ' * (len(kwargs.get('name')) + 2)
                isfirst = False
                line += t + space + ' '
                # add spacing for * alignment
                ns = len([c for c in n if c == '*'])
                line += (' ' * (s - ns)) + n
                data += line
        data += ')\n'
        data += '{\n\t%s\n}\n' % kwargs.get('blob', '').strip()
        return data

    def writeHeader(self, prog, name, stream):
        def w(*a):
            stream.write(' '.join(a) + '\n')
        if prog.license:
            l = prog.license + '.header'
            t = templateFor(l)
            lines = t.render(author='Christian Hergert',
                             year=time.strftime('%Y'),
                             filename=os.path.split(name)[-1]).strip()
            lines = lines.split('\n')
            w('/* ' + '\n * '.join(lines) + '\n */')
	#elif self.project.license:
	#    pass
        else:
            w('/* ', os.path.split(name)[-1], '*/')
        w('')

    def writeStruct(self, prog, klass):
        pc = '%s/%s-%s.c' % (prog.name, prog.prefix, klass.name)
        ph = '%s/%s-%s.h' % (prog.name, prog.prefix, klass.name)

        klass.prepare()

        with self.buildSystem.fileFor(pc) as f:
            self.writeHeader(prog, pc, f)
            t = templateFor('struct.c')
            d = t.render(program=prog, klass=klass)
            f.write(d)

        with self.buildSystem.fileFor(ph) as f:
            self.writeHeader(prog, ph, f)
            t = templateFor('struct.h')
            d = t.render(program=prog, klass=klass)
            f.write(d)

    def writeClass(self, prog, klass):
        pc = '%s/%s-%s.c' % (prog.name, prog.prefix, klass.name)
        ph = '%s/%s-%s.h' % (prog.name, prog.prefix, klass.name)

        klass.prepare()

        for prop in klass.properties.itervalues():
            prop.prepare()

            t = None

            if prop.type_name == 'string':
                prop.field_type = 'gchar*'
                t = templateFor('prop-string.c')
            elif prop.type_name == 'string[]':
                prop.field_type = 'gchar**'
                t = templateFor('prop-string-array.c')
            elif prop.type_name in WORD_TYPES:
                if prop.type_name[0].lower() != 'g':
                    prop.field_type = 'g' + prop.type_name
                else:
                    prop.field_type = prop.type_name
                t = templateFor('prop-gwords.c')
            else:
                print 'missing type info for property', prop.name
                prop.field_type = 'gpointer'

            if t:
                state = {}
                d = t.render(program=prog, klass=klass, prop=prop, state=state)
                prop.rendered = d
                prop.prototype = state.get('prototype')
            else:
                prop.rendered = ''
                prop.prototype = ''

        with self.buildSystem.fileFor(pc) as f:
            self.writeHeader(prog, pc, f)
            t = templateFor('class.c')
            d = t.render(program=prog, klass=klass)
            f.write(d)

        with self.buildSystem.fileFor(ph) as f:
            self.writeHeader(prog, ph, f)
            t = templateFor('class.h')
            d = t.render(program=prog, klass=klass)
            f.write(d)

WORD_TYPES = ('float', 'int', 'uint', 'double', 'long', 'short',
              'ushort', 'ulong', 'int64', 'uint64', 'GPid')
