#!/usr/bin/env python

#
# This file belongs to humanity and ever other intellegent life form
# that can be found in the cosmos.  It's distribution is unlimited
# and unrestricted.  The ideas provided within this file are owned by no-one
# as they are a product of everyone and every state the universe ever was
# and ever will be.
#

import getopt
from   mako.lookup import TemplateLookup
import os
import sys

LOOKUP_PATHS = [os.path.join(os.path.dirname(__file__), 'templates')]
lookup = TemplateLookup(LOOKUP_PATHS)

print LOOKUP_PATHS

def rawTemplateFor(name):
    for prefix in LOOKUP_PATHS:
        p = os.path.join(prefix, name)
        if os.path.exists(p):
            return file(p)

def templateFor(name):
    if not name.endswith('.tmpl'):
        name += '.tmpl'
    return lookup.get_template(name)

def usage(argv=sys.argv, stream=sys.stdout):
    print >> stream, 'Usage:'
    print >> stream, '  %s [OPTIONS...] FILENAME' % argv[0]
    print >> stream, ''
    print >> stream, 'Application Options:'
    print >> stream, '  -h, --help       Show this help menu'
    print >> stream, '  -c, --gobjectc   Generate GObject C source code'
    print >> stream, '  -a, --autotools  Use autotools build system'
    print >> stream, ''

def main(argv=sys.argv, stdout=sys.stdout, stderr=sys.stderr):
    try:
        shortOpts = 'h'
        longOpts = ['help']
        opts, args = getopt.getopt(argv[1:], shortOpts, longOpts)
    except getopt.GetoptError:
        usage(argv, stderr)
        return 1

    if not args:
        usage(argv, stderr)
        return 1

    # defaults for now since they are our primary goal
    useAutotools = True
    useGobjectc = True

    for o,a in opts:
        if o in ('-h', '--help'):
            usage(argv)
            return 0

    projectWriter = None
    buildSystem = None

    from twisted.plugin import log
    log.startLogging(sys.stdout)

    if useGobjectc:
        from cosmos.plugins.cproject import CProjectWriter
        projectWriter = CProjectWriter()

    if useAutotools:
        from cosmos.plugins.autotools import Autotools
        projectWriter.buildSystem = Autotools()

    from cosmos.plugins.dotgraph import DotGraphParser

    projectParser = DotGraphParser()
    parsedProject = projectParser.parse(args[0])
    projectWriter.write(parsedProject)

if __name__ == '__main__':
    sys.exit(main())
