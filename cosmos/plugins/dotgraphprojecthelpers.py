#!/usr/bin/env python

#
# This file belongs to humanity and ever other intellegent life form
# that can be found in the cosmos.  It's distribution is unlimited
# and unrestricted.  The ideas provided within this file are owned by no-one
# as they are a product of everyone and every state the universe ever was
# and ever will be.
#

from cosmos.plugins.dotgraph import *
from twisted.plugin          import IPlugin
from zope.interface          import implements

class ProjectSupport(DotGraphSupportBase):
    def do_support(self, graph, project):
        names = []

        # try to figure out what the name of the project is.
        # most of my projects have a common prefix, so we can look
        # for the executables. ('perfkit-daemon', 'perfkit', etc)
        for n in dotwalk(graph):
            name = get_tag(n, 'name')
            if name:
                names.insert(0, name)

        # for now we'll cheat. must fix this later
        name = names[0].split('-')[0]
        print 'Choose name "%s", from' % name, names
        project.name = name


ProjectSupport = ProjectSupport()
