#!/usr/bin/env python

#
# This file belongs to humanity and ever other intellegent life form
# that can be found in the cosmos.  It's distribution is unlimited
# and unrestricted.  The ideas provided within this file are owned by no-one
# as they are a product of everyone and every state the universe ever was
# and ever will be.
#

from   twisted.plugin    import getPlugins, IPlugin
from   twisted.python    import log
from   zope.interface    import Interface, implements
from   cosmos.interfaces import InFileParser, Project
from   cosmos            import plugins
import pydot

class DotGraphParser(object):
    implements(InFileParser)

    def parse(self, fileName):
        project = DotGraphProject()

        # parse the dotfile
        self.graph = pydot.graph_from_dot_file(fileName)
        self.graph.project = project

        # parse tags from dot file
        self.graph.tags = self.parseTags(fileName)

        ensureSupported('dotgraphprojecthelpers.ProjectSupport', self.graph, project)

        # allow support to find and add more items
        for support in getPlugins(DotGraphSupport, package=plugins):
            support.support(self.graph, project)

        return project

    def canParse(self, fileName):
        pass

    def parseTags(self, fileName):
        def parseTagLine(line):
            name, leftover = line.split(' ', 1)
            tags = []
            tag = ''
            lastSlash = False
            for c in leftover:
                if c == '\\':
                    lastSlash = True
                elif not lastSlash and c in (',',):
                    tags.append(tag)
                    tag = ''
                elif (lastSlash and c in (',', 'n')) or not lastSlash:
                    tag += c
                    lastSlash = False
                else:
                    print 'unknown escape', c
            if tag:
                tags.append(tag)
            return name, tags

        tags = {}
        inTags = False

        for line in file(fileName):
            line = line.strip()
            if line == "/* __DRONEVERSE_TAGS_BEGIN__ */":
                inTags = True
            elif line == "/* __DRONEVERSE_TAGS_END__ */":
                inTags = False
            elif inTags:
                name, lineTags = parseTagLine(line)
                if name not in tags:
                    tags[name] = []
                tags[name].extend(lineTags)
        return tags

class DotGraphProject(object):
    implements(Project)

    def __init__(self):
        self.name = ''
        self.authors = []
        self.license = 'GPL-3'
        self.slogan = ''
        self.libraries = {}
        self.programs = {}
        self.classes = []
        self.structs = []

class DotGraphSupport(Interface):
    def support(self, project, graph):
        pass

class DotGraphSupportBase(object):
    implements(IPlugin, DotGraphSupport)
    has_run = False

    def do_support(self, project, graph):
        pass

    def support(self, project, graph):
        if not self.has_run:
            self.do_support(project, graph)
            self.has_run = True

def dotwalk(graph):
    for node in graph.get_nodes():
        node.graph = graph
        node.project = graph.project
        yield node

def has_tag(node, tags):
    if not node or not tags:
        return False
    if type(tags) == str:
        tags = (tags,)
    for t in tags:
        if get_tag(node, t) != None:
            return True
    return False

def get_tag(node, tag):
    if not node or not tag:
        return None

    t = tag.strip()
    n = node.get_name()

    # print 'has_tag(' + n + ', ' + t + ')'

    if n in node.graph.tags:
        for k in node.graph.tags[n]:
            tn = k.split(':', 1)[0]
            if tn.strip() == t:
                p = k.split(':', 1)
                if len(p) == 2:
                    return p[1]
                return p[0]
    return None

def get_node(graph, name):
    for node in graph.get_nodes():
        if node.get_name() == name:
            node.graph = graph
            node.project = graph.project
            return node

def looks_upon_tagged(node, tag):
    """Checks to see if the node looks at any other node
    with the tag @tag."""
    for e in node.graph.get_edges():
        if e.get_source() == node.get_name():
            n = get_node(node.graph, e.get_destination())
            if n and has_tag(n, tag):
                return True
    return False

def ensureSupported(name, graph, project):
    name = 'cosmos.plugins.' + name
    mod = name[:name.rindex('.')]
    name = name[name.rindex('.')+1:]
    p = __import__(mod, fromlist=[name])
    getattr(p, name).support(graph, project)

def i_see_tagged(node, tags):
    n = node.get_name()
    s = set()
    for edge in node.graph.get_edges():
        if edge.get_source() == n:
            this = get_node(node.graph, edge.get_destination())
            for tag in tags:
                if has_tag(this, tag):
                    s.add(this)
                    break
    return tuple(s)

def who_gazes_upon_me_tagged(node, tags, hash=None):
    # create reverse index by endpoint of link
    if not hash:
        hash = {}
        for e in node.graph.get_edges():
            d = e.get_destination()
            s = e.get_source()
            if d not in hash:
                hash[d] = []
            hash[d].insert(0, s)

   # find everyone looking at me
    n = node.get_name()

    gazed = set()

    if has_tag(node, tags):
        gazed.add(n)

    if n in hash:
        for next in hash[n]:
            next = get_node(node.graph, next)
            for i in who_gazes_upon_me_tagged(next, tags, hash):
                gazed.add(i)

    return tuple(gazed)
