#!/usr/bin/env python

#
# This file belongs to humanity and ever other intellegent life form
# that can be found in the cosmos.  It's distribution is unlimited
# and unrestricted.  The ideas provided within this file are owned by no-one
# as they are a product of everyone and every state the universe ever was
# and ever will be.
#

from   __future__        import with_statement
from   cosmos            import templateFor, rawTemplateFor
from   cosmos.interfaces import BuildSystem
import os
import time
from   zope.interface    import implements

class Autotools(object):
    implements(BuildSystem)

    root = ''
    files = None
    project = None
    features = None

    def __init__(self):
        self.features = {}
        self.files = []

    def prepare(self, ast):
        self.project = ast
        path = ast.name.replace('/','')
        if os.path.exists(ast.name):
            # ask user if we can delete
            os.system('rm -rf ' + ast.name)
        os.mkdir(path)
        self.root = path

    def addFeature(self, name, info=None):
        if not name:
            raise AttributeError('Invalid feature "%s"' % name)
        self.features[name] = info

    def addConfigure(self):
        with self.fileFor('configure.ac') as f:
            def h(comment):
                f.write('dnl ' + '*' * 72 + '\n')
                f.write('dnl ' + comment  + '\n')
                f.write('dnl ' + '*' * 72 + '\n')

            def w(line):
                f.write(line + '\n')

            lower_name = self.project.name.lower().replace('-','_')
            upper_name = self.project.name.upper().replace('-','_')

            # versioning macros
            h('Versioning')
            v = self.getVersion()
            w('m4_define([%s_major_version], [%d])' % (lower_name, v[0]))
            w('m4_define([%s_minor_version], [%d])' % (lower_name, v[1]))
            w('m4_define([%s_micro_version], [%d])' % (lower_name, v[2]))
            w('m4_define([%s_version],' % lower_name)
            w('          [%s_major_version.%s_minor_version.%s_micro_version])' % ((lower_name,) * 3))
            w('m4_define([%s_binary_age], [m4_eval(100 * %s_minor_version +' % ((lower_name,) * 2))
            w('           %s_micro_version)])' % lower_name)
            w('m4_define([lt_current], [m4_eval(100 * %s_minor_version +' % lower_name)
            w('           %s_micro_version - %s_interface_age)])' % ((lower_name,) * 2))
            w('m4_define([lt_revision], [%s_interface_age])' % lower_name)
            w('m4_define([lt_age], [m4_eval(%s_binary_age - %s_interface_age)])' % ((lower_name,) * 2))
            w('%s_MAJOR_VERSION=%s_major_version' % (upper_name, lower_name))
            w('%s_MINOR_VERSION=%s_minor_version' % (upper_name, lower_name))
            w('%s_MICRO_VERSION=%s_micro_version' % (upper_name, lower_name))
            w('')
            w('')

            h('Initialize Automake')
            w('AC_INIT(%s, %s_version)' % ((lower_name,) * 2))
            w('AM_INIT_AUTOMAKE(AC_PACKAGE_NAME, AC_PACKAGE_VERSION)')
            w('AC_CONFIG_HEADERS([config.h])')
            w('AC_CONFIG_MACRO_DIR([build/m4])')
            w('AM_MAINTAINER_MODE')
            w('AC_ISC_POSIX')
            w('AC_PROG_CC')
            w('AC_PROG_CC_STDC')
            w('AC_HEADER_STDC')
            w('AM_PATH_GLIB_2_0')
            w('AC_SUBST(%s_MAJOR_VERSION)' % upper_name)
            w('AC_SUBST(%s_MAJOR_VERSION)' % upper_name)
            w('AC_SUBST(%s_MAJOR_VERSION)' % upper_name)
            w('')
            w('')

            h('Check for Required Modules')
            if 'lang:c' in self.features:
                for prog in self.project.programs.itervalues():
                    upper_name = prog.name.upper().replace('-','_')
                    space = len(upper_name) * ' '
                    w('PKG_CHECK_MODULES(%s, [gobject-2.0 >= 2.16' % upper_name)
                    if 'threading' in prog.features:
                        w('                  %s   gthread-2.0 >= 2.16' % space)
                    if 'gtk' in prog.features:
                        w('                  %s   gtk+-2.0    >= 2.18' % space)
                    if 'plugins' in prog.features:
                        w('                  %s   ethos-1.0   >= 0.2.1' % space)
                    w('                  %s   dbus-glib-1 >= 0.80])' % space)
            w('')
            w('')

            h('Unit Tests')
            w('AC_PATH_PROG([GTESTER], [gtester])')
            w('AC_PATH_PROG([GTESTER_REPORT], [gtester-report])')
            w('AM_CONDITIONAL(ENABLE_GLIB_TEST, test "x$enable_glibtest" = "xyes")')
            w('')
            w('')

            h('Miscellaneous')
            w('AM_PROG_LIBTOOL')
            w('GTK_DOC_CHECK([1.11])')
            w('SHAVE_INIT([build], [enable])')
            w('SHAMROCK_EXPAND_LIBDIR')
            w('SHAMROCK_EXPAND_BINDIR')
            w('SHAMROCK_EXPAND_DATADIR')
            w('')
            w('')

            h('Output')
            w('AC_OUTPUT([')
            w('\tMakefile')
            if 'lang:c' in self.features:
                w('\tbuild/shave')
                w('\tbuild/shave-libtool')
                w('\tdata/Makefile')
                if 'dbus' in self.features:
                    w('\tdata/dbus/Makefile')
                w('\tdoc/Makefile')
                if 'doc:gtk' in self.features:
                    w('\tdoc/reference/Makefile')
                w('\ttests/Makefile')
                for prog in self.project.programs.itervalues():
                    w('')
                    if 'doc:gtk' in self.features:
                        w('\tdoc/reference/%s/Makefile' % prog.name)
                        w('\tdoc/reference/%s/version.xml' % prog.name)
                    w('\t%s/Makefile' % prog.name)
                    w('\t%s/%s-version.h' % (prog.name, prog.prefix))
            w('])')
            w('')
            w('echo ""')
            w('echo "%s $VERSION"' % self.project.name.capitalize())
            w('echo ""')
            w('echo "  Prefix.....................: ${prefix}"')
            if 'lang:c' in self.features:
                if 'doc:gtk' in self.features:
                    w('echo "  Enable API Reference.......: ${enable_gtk_doc}"')
                w('echo "  Enable Test Suite..........: ${enable_glibtest}"')
            f.write('echo ""\n')

    def complete(self):
        with self.fileFor('AUTHORS') as f:
            # I don't particularly need anyone else, but maybe
            # it would be nice to support it someday :-)
            f.write('Christian Hergert <chris@dronelabs.com>\n')

        with self.fileFor('ChangeLog') as f:
            date = time.strftime('%Y-%m-%d')
            f.write('%s\tChristian Hergert\t<chris@dronelabs.com>\n' % date)
            f.write('\n')
            f.write("\t* .: The ChangeLog is obsolete.  Please use `git log'\n")
            f.write('\tfor information on changes.\n')
            f.write('\n')

        with self.fileFor('autogen.sh', mode='rwx') as f:
            t = templateFor('autogen.sh')
            d = t.render(**{'project': self.project})
            f.write(d.strip())

        # project wide license
        if self.project.license:
            d = self.getLicenseData(self.project.license)
            self.fileFor('COPYING').write(d)

        # add special licenses if necessary
        items = [(p.name, p.license) for p in self.project.programs.itervalues()]
        items.extend([(p.name, p.license) for p in self.project.libraries.itervalues()])
        for name,license in items:
            if license != self.project.license:
                with self.fileFor('COPYING.'+name) as f:
                    d = self.getLicenseData(license)
                    f.write(d)

        self.fileFor('NEWS').write('')
        self.fileFor('README').write('')

        # copy over some build system files
        self.copyTo('acinclude.m4', 'acinclude.m4')
        self.copyTo('shave.in', 'build/shave.in')
        self.copyTo('shave-libtool.in', 'build/shave-libtool.in')
        self.copyTo('expansions.m4', 'build/m4/expansions.m4')

        # build our configure script
        self.addConfigure()

        # setup unit testing
        self.addTests()

        # setup our programs
        for prog in self.project.programs.itervalues():
            self.addMakefile(prog)
            self.addDocs(prog)

        # doc toplevel makefile
        with self.fileFor('doc/Makefile.am') as f:
            subdirs = ' '.join([t[2] for t in self.subdirsIn('doc')])
            f.write('SUBDIRS = ' + subdirs + '\n')

        if 'doc:gtk' in self.features:
            with self.fileFor('doc/reference/Makefile.am') as f:
                subdirs = ' '.join([t[3] for t in self.subdirsIn('doc/reference')])
                f.write('SUBDIRS = ' + subdirs + '\n')

        with self.fileFor('data/Makefile.am') as f:
            if 'dbus' in self.features:
                f.write('SUBDIRS = dbus\n')

        if 'dbus' in self.features:
            f = self.fileFor('data/dbus/Makefile.am')
            f.write('EXTRA_DIST = \\\n\t')
            for klass in self.project.classes:
                pn = 'com.dronelabs.%s.%s' % (prog.name.split('-')[0].capitalize(),
                                              klass.camel_name)
                f.write('%s.xml \\\n\t' % pn)
                with self.fileFor('data/dbus/' + pn + '.xml') as fx:
                    fx.write('<?xml version="1.0" ?>\n')
                    fx.write("""<node>
  <interface name="%(objname)s">
    <annotation name="org.freedesktop.DBus.GLib.CSymbol" value="%(lower_prefix)s"/>\n""" % {
    'objname': pn,
    'lower_prefix': prog.lower_name + '_' + klass.lower_name,
})
                    if klass.properties:
                        fx.write('\n    <!-- Properties -->\n')
                    for prop in klass.properties.itervalues():
                        if prop.type_name == 'string':
                            fx.write('    <property name="%s" type="s" access="readwrite" />\n' % prop.camel_name)
                        elif prop.type_name == 'int':
                            fx.write('    <property name="%s" type="u" access="readwrite" />\n' % prop.camel_name)
                        elif prop.type_name == 'string[]':
                            fx.write('    <property name="%s" type="as" access="readwrite" />\n' % prop.camel_name)

                    if klass.methods:
                        fx.write('\n    <!-- Methods -->\n')
                        for method in klass.methods:
                            fx.write('    <method name="%s">\n' % method.name.capitalize())
                            fx.write('      <annotation name="org.freedesktop.DBus.GLib.CSymbol" value="%s_%s_%s_dbus"/>\n' % (
                                prog.lower_name, klass.lower_name, method.name.lower()))
                            fx.write('    </method>\n')
                    fx.write('  </interface>\n</node>\n')
            f.write('$(NULL)\n')

        # main makefile
        self.addToplevelMakefile()

    def addDocs(self, prog):
        if 'lang:c' in self.features:
            if 'doc:gtk' in self.features:
                p = 'doc/reference/%s/Makefile.am' % prog.name
                t = rawTemplateFor('Makefile.gtk-doc')
                with self.fileFor(p) as f:
                    d = t.read()
                    d = d.replace('##name##', prog.lower_name)
                    d = d.replace('##NAME##', prog.upper_name)
                    d = d.replace('##prog##', prog.name)
                    d = d.replace('##PROG##', prog.name.replace('-','_').upper())
                    f.write(d)
                p = 'doc/reference/%s/version.xml.in' % prog.name
                with self.fileFor(p) as f:
                    f.write('.'.join([str(i) for i in self.getVersion()]))

    def getVersion(self):
        return 0, 1, 1

    def subdirsIn(self, name='.'):
        if name != '.':
            prefix = [a for a in os.path.split(name) if a]
            prefix.insert(0, self.project.name)
            prefix = tuple(prefix)
        else:
            prefix = (self.project.name,)
        items = set()
        for file in self.files:
            this = file[:len(prefix)]
            if tuple(prefix) == tuple(this):
                this = file[:len(prefix)+1]
                if os.path.isdir(os.path.join(*this)):
                    items.add(tuple(this))
        return list(items)

    def subfilesIn(self, name='.'):
        if name != '.':
            prefix = [a for a in os.path.split(name) if a]
            prefix.insert(0, self.project.name)
            prefix = tuple(prefix)
        else:
            prefix = (self.project.name,)
        items = set()
        for file in self.files:
            this = file[:len(prefix)]
            if tuple(prefix) == tuple(this):
                this = file[:len(prefix)+1]
                if os.path.isfile(os.path.join(*this)):
                    items.add(tuple(this))
        return list(items)

    def addToplevelMakefile(self):
        # add our top-level make file. do this at the end so that
        # we have a list of all our toplevel directories.
        toplevels = set()
        for t in self.subdirsIn():
            if t[1] == 'build':
                continue
            toplevels.add(t[1])

        # fix for docs quick
        toplevels.remove('doc')
        toplevels.remove('tests')

        with self.fileFor('Makefile.am') as f:
            def w(*a):
                f.write(' '.join(a) + '\n')
            w('NULL = ')
            w('SUBDIRS = %s tests doc' % (' '.join(toplevels)))
            w('ACLOCAL_AMFLAGS = -I build/m4')
            flags = '--enable-maintainer-flags'
            if 'doc:gtk' in self.features:
                flags += ' --enable-gtk-doc'
            w('DISTCHECK_CONFIGURE_FLAGS = ' + flags)
            w('')
            w(self.project.name + 'docdir = ${prefix}/doc/' + self.project.name)
            w(self.project.name + 'doc_DATA = \\')
            w('\tREADME    \\')
            w('\tCOPYING   \\')
            w('\tAUTHORS   \\')
            w('\tChangeLog \\')
            w('\tINSTALL   \\')
            w('\tNEWS      \\')
            w('\t$(NULL)')
            w('')
            w('EXTRA_DIST = $(' + self.project.name + 'doc_DATA)')
            w('')
            f.write("""dist-hook:
	@if test -d "$(srcdir)/.git"; then                              \\
          (cd "$(srcdir)" &&                                            \\
           $(top_srcdir)/missing --run git log --stat ) > ChangeLog.tmp \\
           && mv -f ChangeLog.tmp $(top_distdir)/ChangeLog              \\
           || (rm -f ChangeLog.tmp;                                     \\
               echo Failed to generate ChangeLog >&2);                  \\
	else                                                            \\
	  echo A git checkout is required to generate a ChangeLog >&2;  \\
	fi

uninstall-local:
""")
            w('\t-rm -r $('+self.project.name+'docdir)')

    def addMakefile(self, prog):
        p = lambda n: os.path.join(prog.name, n)
        with self.fileFor(p('Makefile.am')) as f:
            def w(*a):
                f.write(' '.join(a) + '\n')
            domain = ''.join([a.capitalize() for a in prog.name.split('-')])
            upper = prog.name.replace('-','_').upper()
            w('NULL =')
            w('bin_PROGRAMS = ' + prog.name)
            w('noinst_LTLIBRARIES = lib' + prog.name + '.la')
            w('')
            w('AM_CPPFLAGS = \\')
            w('\t-DPACKAGE_LOCALE_DIR=\\""$(prefix)/$(DATADIRNAME)s/locale"\\" \\')
            w('\t-DPACKAGE_SRC_DIR=\\""$(srcdir)"\\" \\')
            w('\t-DPACKAGE_DATA_DIR=\\""$(datadir)"\\" \\')
            w('\t-DPACKAGE_LIB_DIR=\\""$(libdir)"\\" \\')
            w('\t-DGETTEXT_PACKAGE=\\""' + prog.name + '"\\" \\')
            w('\t-DHAVE_CONFIG_H \\')
            w('\t-DG_LOG_DOMAIN=\\"%s\\" \\' % domain)
            w('\t$(%s_CFLAGS) \\' % upper)
            w('\t$(NULL)')
            w('')
            w('AM_LDFLAGS = $(%s_LIBS)' % upper)
            w('')
            w('INST_H_FILES = \\')
            w('\t%s-version.h \\' % prog.prefix)
            exclude = []
            for fName in self.files:
                if fName[1] != prog.name:
                    continue
                elif '-private.h' in fName[-1] or 'priv.h' in fName[-1]:
                    continue
                elif fName[-1].endswith('.h'):
                    exclude.insert(0, fName)
                    w('\t' + os.path.join(*fName[2:]) + ' \\')
            w('\t$(NULL)')
            w('')
            lower = prog.name.replace('-','_')
            w('lib%s_la_SOURCES = \\' % lower)
            w('\t$(INST_H_FILES) \\')
            for fName in self.files:
                if fName[1] != prog.name or fName[-1] == 'main.c':
                    continue
                if (fName not in exclude) and (fName[-1][-2:] in ('.c', '.h')):
                    w('\t' + os.path.join(*fName[2:]) + ' \\')
            w('\t$(NULL)')
            w('')
            w('headerdir = $(prefix)/include/%s-1.0/%s' % ((prog.name,) * 2))
            w('header_DATA = $(INST_H_FILES)')
            w('')
            w('%s_SOURCES = main.c' % (lower,))
            w('%s_LDADD = lib%s.la' % (lower, prog.name))
            w('%s_LDFLAGS = \\' % lower)
            w('\t-export-dynamic \\')
            w('\t-no-undefined \\')
            w('\t-export-symbols-regex "^%s_.*" \\' % prog.prefix.replace('-','_'))
            w('\t$(NULL)')

    def addTests(self):
        self.copyTo('Makefile.decl')
        with self.fileFor('tests/Makefile.am') as f:
            f.write('include $(top_srcdir)/Makefile.decl\n')
            f.write('\n')
            f.write('noinst_PROGRAMS = \n')
            f.write('TEST_PROGS += \n')
            f.write('INCLUDES = -I$(top_srcdir)\n')
            # todo: add programs and libraries
            f.write('#LDADD = \n')
            f.write('AM_CFLAGS = $(%s_CFLAGS)\n' % self.project.name.upper().replace('-','_'))
            f.write('AM_LDFLAGS = $(%s_LIBS)\n' % self.project.name.upper().replace('-','_'))
            f.write('\n')
            # todo: write unit tests for each class/method

    def copyTo(self, srcName, dstName=None):
        if dstName is None:
            dstName = srcName
        self.fileFor(dstName).write(rawTemplateFor(srcName).read())

    def getLicenseData(self, name):
        p = rawTemplateFor(name.upper())
        return p and p.read() or ''

    def fileFor(self, path, mode='rw'):
        x = 'x' in mode
        mode = mode.replace('x','')
        if 'w' in mode:
            mode = 'w'
        p = os.path.join(self.root, path)
        d = os.path.dirname(p)
        if not os.path.exists(d):
            os.makedirs(d)
        parts = p.split('/')
        if parts not in self.files:
            self.files.insert(0, parts)
        f = file(p, mode)
        if x:
            os.chmod(f.name, 0755)
        return f

    def addDirectory(self, *path):
        d = os.path.join(self.root, *path)
        if not os.path.exists(d):
            os.makedirs(d)
        elif not os.path.isdir(d):
            raise ValueError, 'Ugh! %s exists and not a path' % d
